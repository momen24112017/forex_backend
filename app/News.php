<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class News extends Model
{
    protected $fillable = [
        'news_categories_id', 'title', 'body', 'image', 'important', 'tags'
    ];
    
    public $MediaUrl = "https://forex-steps.com/storage/app/public/";

    public function newsDetails($id){
        $result = News::where('id', $id)->get();
        foreach($result as $image=>$value){
            $value['image'] = $this->MediaUrl.$value['image'];
        }
        $string = str_replace(" ", "", $result[0]['tags']);
        $string = strtolower($string);
        $arrTags = explode(",",$string);
        $result[0]['tags'] = $arrTags;
        return $result;
    }

    public function getImportantNews(){
        $result = News::where('important', 1)->orderBy('id', 'DESC')->get();
        foreach($result as $image=>$value){
            $value['image'] = $this->MediaUrl.$value['image'];
        }
        return $result;
    }

    public function relatedNews($id){
        $result = News::where('news_categories_id', $id)->orderBy('id', 'desc')->take(3)->get();
        foreach($result as $image=>$value){
            $value['image'] =  $this->MediaUrl.$value['image'];
        }
        return $result;
    }

    public function relatedNewsCategory($id){
        $result = News::where('news_categories_id', $id)->orderBy('id', 'desc')->paginate(6);
        // whereDate('created_at', '>', Carbon::now()->subDays(30))->orderBy('id', 'DESC')
        foreach($result as $image=>$value){
            $value['image'] =  $this->MediaUrl.$value['image'];
        }
        return $result;
    }
    
    public function similarNews($newsTitle){
        $result = News::where('title','LIKE','%'.$newsTitle.'%')->orderBy('id', 'desc')->take(3)->get();
        foreach($result as $image=>$value){
            $value['image'] = $this->MediaUrl.$value['image'];
        }
        return $result;
    }

    public function newsSameTag($tag){
        $result = News::where('tags','LIKE','%'.$tag.'%')->get();
        foreach($result as $image=>$value){
            $value['image'] =  $this->MediaUrl.$value['image'];
        }
        return $result;
    }

    public function getLatestNews(){
        $result = News::latest()->orderBy('id', 'DESC')->get('title');
        return $result;
    }

}
